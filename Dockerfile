FROM openjdk:12
ADD target/docker-spring-boot-alexandre_nazario.jar docker-spring-boot-alexandre_nazario.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-alexandre_nazario.jar"]
